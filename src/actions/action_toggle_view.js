import { LIST_VIEW, DIALOG_OPEN, DIALOG_CLOSE } from "./action_types";

export const isListView = () => ({
    type: LIST_VIEW
});

export const openDialog = () => ({
    type: DIALOG_OPEN
});

export const closeDialog = () => ({
    type: DIALOG_CLOSE
});
