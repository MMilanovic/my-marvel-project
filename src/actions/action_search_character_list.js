import { SEARCH_BY_NAME } from "./action_types";
import { apiService } from "../services/apiService";
import Character from "../entities/Character";

const setCharacterList = (characterList = []) => ({
    type: SEARCH_BY_NAME,
    characterList
})

export const startSearchCharacters = (searchValue) => {
    return dispatch => {
        if (!searchValue) {
            return dispatch(setCharacterList());
        }
        return apiService.searchCharacters(searchValue)
            .then(resultList => {
                const characterList = resultList.map(resultItem => new Character(resultItem));
                return dispatch(setCharacterList(characterList))
            })
            .catch(error => {
                console.log(error);
            })
    }
}