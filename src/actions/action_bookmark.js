import { BOOKMARK_CHARACTER, UNBOOKMARK_CHARACTER } from "./action_types";

export const bookmarkCharacter = (bookmark = {}) => ({
    type: BOOKMARK_CHARACTER,
    bookmark
});

export const unbookmarkCharacter = ({ id } = {}) => ({
    type: UNBOOKMARK_CHARACTER,
    id
})