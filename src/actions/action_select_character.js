import { SELECT_CHARACTER } from "./action_types";

export const selectCharacter = selectedCharacter => ({
    type: SELECT_CHARACTER,
    selectedCharacter
});