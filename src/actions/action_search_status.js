import {
    SEARCH_STARTED,
    SEARCH_FINISHED,
    SEARCH_EMPTY,
    SEARCH_POPULATED,
    SPINNER
} from "./action_types";

export const searchStarted = () => ({
    type: SEARCH_STARTED
});

export const searchFinished = () => ({
    type: SEARCH_FINISHED
});

export const searchEmpty = () => ({
    type: SEARCH_EMPTY
});

export const searchPopulated = () => ({
    type: SEARCH_POPULATED
});

export const displaySpinner = () => ({
    type: SPINNER
})