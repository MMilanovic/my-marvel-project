import { SEARCH_BY_NAME } from "../actions/action_types";

const defaultState = [];

export default (state = defaultState, action) => {
    switch (action.type) {
        case SEARCH_BY_NAME:
            return action.characterList;
        default:
            return state;
    }
};