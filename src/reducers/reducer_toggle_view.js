import { LIST_VIEW, DIALOG_OPEN, DIALOG_CLOSE } from "../actions/action_types";

const defaultState = {
    list: true,
    dialog: false
};

export default (state = defaultState, action) => {
    switch (action.type) {
        case LIST_VIEW:
            return {
                ...state,
                list: !state.list
            };
        case DIALOG_OPEN:
            return {
                ...state,
                dialog: true
            };
        case DIALOG_CLOSE:
            return {
                ...state,
                dialog: false
            };
        default:
            return state;
    }
};