import { SELECT_CHARACTER } from "../actions/action_types";

const defaultState = {};

export default (state = defaultState, action) => {
    switch (action.type) {
        case SELECT_CHARACTER:
            return action.selectedCharacter
        default:
            return state;
    }
};