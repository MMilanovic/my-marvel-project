import {
    SEARCH_FINISHED,
    SEARCH_STARTED,
    SEARCH_EMPTY,
    SEARCH_POPULATED,
    SPINNER
} from "../actions/action_types";

const defaultState = {
    isSearchFinished: false,
    emptyResult: false,
    spinner: false
};

export default (state = defaultState, action) => {
    switch (action.type) {
        case SEARCH_FINISHED:
            return {
                ...state,
                isSearchFinished: true,
                spinner: false
            };
        case SEARCH_STARTED:
            return {
                ...state,
                finished: false
            };
        case SEARCH_EMPTY:
            return {
                ...state,
                emptyResult: true
            };
        case SEARCH_POPULATED:
            return {
                emptyResult: false
            };
        case SPINNER:
            return {
                spinner: true
            };
        default:
            return state;
    }
};