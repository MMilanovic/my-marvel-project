import { BOOKMARK_CHARACTER, UNBOOKMARK_CHARACTER } from "../actions/action_types";

const defaultState = [];

export default (state = defaultState, action) => {
    switch (action.type) {
        case BOOKMARK_CHARACTER:
            const newState = state.filter(item => {
                return item.id !== action.bookmark.id;
            })
            return [
                ...newState,
                action.bookmark
            ];
        case UNBOOKMARK_CHARACTER:
            return state.filter(item => {
                return item.id !== action.id
            });
        default:
            return state;
    }
};