import React from "react";
import CharacterItem from "../containers/CharacterItem";

const CharacterListResult = ({ list, isList }) => {
    return (
        <div className="characters-items">
            {list.map(item => {
                return (
                    <CharacterItem
                        item={item}
                        list={isList}
                        key={item.id}
                    />
                )
            }
            )}
        </div>
    );
};

export default CharacterListResult;