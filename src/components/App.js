import React, { Component } from "react";
import SearchBar from "../containers/SearchBar";
import CharacterList from "../containers/CharacterList";
import Header from "./partials/Header";
import { library } from "@fortawesome/fontawesome-svg-core";
import { faList, faTh, faTimes } from "@fortawesome/free-solid-svg-icons";
import Footer from "./partials/Footer";
import SpinnerContainer from "../containers/SpinnerContainer";

library.add(faList, faTh, faTimes);

class App extends Component {
  render() {
    return (
      <div className="container">
        <Header />
        <div className="container__app" >
          <SearchBar />
          <SpinnerContainer />
          <CharacterList />
        </div>
        <Footer />
      </div>
    );
  }
}

export default App;
