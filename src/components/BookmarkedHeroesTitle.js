import React from "react";

const BookmarkedHeroesTitle = ({ title }) => {
    return (
        <div className="bookmarked-heroes-title">
            <h4>{title}</h4>
        </div>
    );
};
export default BookmarkedHeroesTitle;