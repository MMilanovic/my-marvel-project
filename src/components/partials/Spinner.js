import React from "react";


const Spinner = () => {
    const style = { "width": "100%", "height": "100%" };

    return (
        <div className="spinner">
            <div className="lds-css ng-scope">
                <div
                    style={style}
                    className="lds-dual-ring">
                    <div></div>
                </div>
            </div>
        </div>
    );
};

export default Spinner;