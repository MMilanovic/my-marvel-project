import React from "react";
import ToggleViewButton from "../../containers/ToggleViewButton";

const Header = props => {
    return (
        <div className="header">
            <div className="header__container">
                <div className="header__container__logo"></div>
            </div>
            <ToggleViewButton />
        </div>
    )
};

export default Header;