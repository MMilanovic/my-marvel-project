
import md5 from "md5";

const PRIVATE_KEY=process.env.REACT_APP_PRIVATE_KEY;
const API_KEY=process.env.REACT_APP_API_KEY;

export const BASE_ENDPOINT = "https://gateway.marvel.com/";
export const TIMESTAMP = new Date().getTime();
export const HASH = md5(`${TIMESTAMP}${PRIVATE_KEY}${API_KEY}`);

// parameters:
export const nameStartsWithParameter = `nameStartsWith=`;

export const searchInputDelay = 2000;
export const ENTER_KEYCODE = 13;
export const aspect_ratio = "standard_xlarge";

// messages:
export const bookmarkedHeroesTitle = "Bookmarked heroes:";
export const lastModifiedMessage = "Updated at :";
export const copyrigthFooterMessage="Ⓒ 2018 copyright Maja";
export const footerMessage="Made using Marvel api";
