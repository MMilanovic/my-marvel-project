export const isIDIncludedInList = (id, list) => {
    let result = false;
    for (let i = 0; i < list.length; i++) {
        if (list[i] === id) {
            result = true;
        }
    };
    return result;
};