import moment from "moment";
import { aspect_ratio } from "../shared/constants";

class Character {
    constructor(character) {
        this.id = character.id;
        this.name = character.name;
        this.description = character.description;
        this.dateModified = moment(character.modified).format('LLL');
        this.image = `${character.thumbnail.path}/${aspect_ratio}.${character.thumbnail.extension}`;
        this.comics = character.comics;
        this.stories = character.stories;
        this.events = character.events;
        this.series = character.series;
    }
};

export default Character;