import axios from "axios";
import { BASE_ENDPOINT, HASH, TIMESTAMP, nameStartsWithParameter } from "../shared/constants";

class ApiService {
    searchCharacters(searchValue) {
        const parameter = nameStartsWithParameter;
        const API_KEY = process.env.REACT_APP_API_KEY;
        const endpoint = `${BASE_ENDPOINT}v1/public/characters?${parameter}${searchValue}&ts=${TIMESTAMP}&apikey=${API_KEY}&hash=${HASH}&limit=20`;
        return axios.get(endpoint)
            .then(response => response.data.data.results)
    };
};

export const apiService = new ApiService();