import React from "react";
import { connect } from "react-redux";
import { isListView } from "../actions/action_toggle_view";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

const ToggleViewButton = props => {
    const isCharacterListPopulated = () => {
        if (props.characterList.length !== 0) {
            return true;
        }
        if (props.bookmarkList.length !== 0 && props.searchFinished) {
            return true;
        }
        return false;
    };
    
    return (
        <div className="toggle-container">
            {isCharacterListPopulated() && <div onClick={props.isListView}>
                <FontAwesomeIcon
                    icon={!props.view ? "list" : "th"}
                    className="toggle-icon"
                />
            </div>}
        </div>
    );
};

const mapStateToProps = state => ({
    view: state.toggle.list,
    characterList: state.characterList,
    bookmarkList: state.bookmarkList,
    searchFinished: state.searchFinished.isSearchFinished
});

const mapDispatchToProp = dispatch => ({
    isListView: () => dispatch(isListView()),
})

export default connect(mapStateToProps, mapDispatchToProp)(ToggleViewButton);
