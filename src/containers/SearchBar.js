import React, { Component } from "react";
import { connect } from "react-redux";
import { searchInputDelay, ENTER_KEYCODE } from "../shared/constants";
import { startSearchCharacters } from "../actions/action_search_character_list";
import { searchFinished, searchEmpty, searchPopulated, displaySpinner } from "../actions/action_search_status";

class SearchBar extends Component {
    state = {
        searchValue: "",
    };

    timeDelay = null;

    onInputSearch = e => {
        const { searchEmpty, searchFinished, displaySpinner } = this.props;
        const searchValue = e.target.value;
        if (this.timeDelay) {
            clearTimeout(this.timeDelay);
        };
        this.setState(() => ({ searchValue }));

        if (!searchValue) {
            searchEmpty();
            searchFinished();
        };

        if (searchValue) {
            displaySpinner();
            this.timeDelay = setTimeout(() => this.startSearch(searchValue), searchInputDelay);
        };

    };

    startSearch = () => {
        const { searchValue } = this.state;
        const { startSearchCharacters, searchPopulated, searchFinished } = this.props;

        searchPopulated();
        startSearchCharacters(searchValue)
            .then(() => {
                searchFinished();
            })
    };

    onKeyDownSearch = e => {
        const searchValue = e.target.value;
        if (searchValue) {
            if (e.keyCode === ENTER_KEYCODE) {
                clearTimeout(this.timeDelay);
                this.startSearch();
            }
        }
    };

    render() {
        const { searchValue } = this.state;
        return (
            <div className="search__container">
                <input
                    className="search-input"
                    name="search"
                    onChange={this.onInputSearch}
                    onKeyDown={this.onKeyDownSearch}
                    type="search"
                    placeholder="Search characters..."
                    value={searchValue}
                    autoComplete="off"
                />
            </div>
        );
    };
};

const mapDispatchToProps = dispatch => ({
    startSearchCharacters: (value) => dispatch(startSearchCharacters(value)),
    searchFinished: () => dispatch(searchFinished()),
    searchEmpty: () => dispatch(searchEmpty()),
    searchPopulated: () => dispatch(searchPopulated()),
    displaySpinner: () => dispatch(displaySpinner())
})

export default connect(null, mapDispatchToProps)(SearchBar);