import React from "react";
import BookmarkButton from "../containers/BookmarkButton";
import { connect } from "react-redux";
import { openDialog } from "../actions/action_toggle_view";
import { selectCharacter } from "../actions/action_select_character";


const CharacterItem = ({ item, list = "list", openDialog, selectCharacter }) => {
    const backgroundImage = { backgroundImage: `url(${item.image})` };

    const display = list ? "list" : "card";

    const onOpenDialog = e => {
        selectCharacter(item);
        openDialog();
    };
    return (
        <div className={`character-${display}`}>
            <div
                className={`character-${display}__thumb`}
                style={backgroundImage}
            >
            </div>
            <div className={`character-${display}__content`}>
                <h1 onClick={onOpenDialog}>{item.name}</h1>
                <BookmarkButton character={item} />
            </div>
        </div>
    );
};

const mapDispatchToProps = dispatch => ({
    openDialog: () => dispatch(openDialog()),
    selectCharacter: character => dispatch(selectCharacter(character))
});

export default connect(null, mapDispatchToProps)(CharacterItem);