import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { connect } from "react-redux";
import { closeDialog } from "../actions/action_toggle_view";
import { lastModifiedMessage } from "../shared/constants";

const Dialog = ({ selectedCharacter, closeDialog }) => {
    const backgroundImage = { backgroundImage: `url(${selectedCharacter.image})` };

    const onCloseDialog = e => closeDialog();

    return (
        <div className="dialog">
            <div className="dialog__container">
                <div className="dialog__container__content">
                    <span
                        onClick={onCloseDialog}
                    >
                        <FontAwesomeIcon
                            icon={"times"}
                            className="dialog__container__content--icon"
                        />
                    </span>
                    <div className="dialog__container__content--header">
                        <p>{selectedCharacter.name}</p>
                    </div>
                    <div className="dialog__container__content--body">
                        <div
                            className="dialog__container__content--body__avatar"
                            style={backgroundImage}
                        >
                        </div>
                        <p> {`${lastModifiedMessage} ${selectedCharacter.dateModified}`}</p>
                        <p>{selectedCharacter.description && selectedCharacter.description}</p>
                    </div>
                </div>
            </div>
        </div>
    );
};

const mapStateToProps = state => ({
    selectedCharacter: state.selectedCharacter
});

const mapDispatchToProps = dispatch => ({
    closeDialog: () => dispatch(closeDialog())
})

export default connect(mapStateToProps, mapDispatchToProps)(Dialog);