import React from "react";
import { connect } from "react-redux";
import Spinner from "../components/partials/Spinner";

const SpinnerContainer = ({ spinner }) => (
    <div>
        {spinner && <Spinner />}
    </div>
);

const mapStateToProps = state => ({
    spinner: state.searchFinished.spinner
});

export default connect(mapStateToProps)(SpinnerContainer);