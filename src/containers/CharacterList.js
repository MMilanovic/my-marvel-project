import React from "react";
import { connect } from "react-redux";
import CharacterListResult from "../components/CharacterListResult";
import BookmarkedHeroesTitle from "../components/BookmarkedHeroesTitle";
import { bookmarkedHeroesTitle } from "../shared/constants";
import Dialog from "./Dialog";

const CharacterList = ({
    characterList,
    bookmarkList,
    list,
    searchEmpty,
    searchFinished,
    isDialogOpen,
    spinner
}) => {
    return (
        <div>
            {isDialogOpen && <Dialog />}
            {characterList.length !== 0 && !spinner && !searchEmpty &&
                <CharacterListResult list={characterList} isList={list} />}
            {characterList.length === 0 && searchFinished &&
                <div>
                    {!searchEmpty && <p className="no-results-message"> 0 characters found.</p>}
                    {bookmarkList.length !== 0 &&
                        <div>
                            <BookmarkedHeroesTitle title={bookmarkedHeroesTitle} />
                            <CharacterListResult list={bookmarkList} isList={list} />
                        </div>
                    }
                </div>}
            {searchEmpty && characterList.length !== 0 && bookmarkList.length !== 0 &&
                <div>
                    <BookmarkedHeroesTitle title={bookmarkedHeroesTitle} />
                    <CharacterListResult list={bookmarkList} isList={list} />
                </div>
            }
        </div>
    );
};

const mapStateToProps = state => ({
    characterList: state.characterList,
    list: state.toggle.list,
    bookmarkList: state.bookmarkList,
    searchFinished: state.searchFinished.isSearchFinished,
    searchEmpty: state.searchFinished.emptyResult,
    isDialogOpen: state.toggle.dialog,
    spinner: state.searchFinished.spinner
});


export default connect(mapStateToProps)(CharacterList);