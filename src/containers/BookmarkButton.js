import React, { Component } from "react";
import { bookmarkCharacter, unbookmarkCharacter } from "../actions/action_bookmark";
import { connect } from "react-redux";
import { isIDIncludedInList } from "../shared/utils";

class BookmarkButton extends Component {
    state = {
        clicked: false
    };
    componentDidMount() {
        if (this.props.bookmarkList.length !== 0) {
            const idList = this.props.bookmarkList.map(item => item.id);
            const isBookmarked = isIDIncludedInList(this.props.character.id, idList);
            this.setState(() => ({ clicked: isBookmarked }));
        }
    }

    onbookmarkCharacter = e => {
        if (!this.state.clicked) {
            this.props.bookmarkCharacter(this.props.character);
            this.setState(() => ({ clicked: true }));
        };
        if (this.state.clicked) {
            const { id } = this.props.character;
            this.props.unbookmarkCharacter({ id });
            this.setState(() => ({ clicked: false }));
        };
    };
    render() {
        const { clicked } = this.state;
        return (
            <div className="bookmark__container">
                <button
                    onClick={this.onbookmarkCharacter}
                    className={!clicked ? "bookmark__container__tab" : "bookmark__container__tab--clicked"}
                >
                    {!clicked ? "Bookmark" : "Bookmarked!"}
                </button>
            </div>
        );
    };
};

const mapStateToProps = state => ({
    bookmarkList: state.bookmarkList
});

const mapDispatchToProps = dispatch => ({
    bookmarkCharacter: item => dispatch(bookmarkCharacter(item)),
    unbookmarkCharacter: ({ id }) => dispatch(unbookmarkCharacter({ id }))
});

export default connect(mapStateToProps, mapDispatchToProps)(BookmarkButton);