import React from 'react';
import ReactDOM from 'react-dom';
import App from './components/App';
import "normalize.css";
import 'core-js/es6/number';
import "./styles/css/styles.css";
import { Provider } from "react-redux";
import configureStore from './store/configureStore';
import registerServiceWorker from './registerServiceWorker';
import { saveState } from "./services/storageService";

const store = configureStore();
store.subscribe(() => {
    saveState({ bookmarkList: store.getState().bookmarkList });
});

const app = (
    <Provider store={store}>
        <App />
    </Provider>
)

ReactDOM.render(app, document.getElementById('root'));
registerServiceWorker();
