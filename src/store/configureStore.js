import { createStore, combineReducers, applyMiddleware } from "redux";
import thunk from 'redux-thunk';
import searchCharacterListReducer from "../reducers/reducer_search_character_list";
import displayReducer from "../reducers/reducer_toggle_view";
import bookmarkReducer from "../reducers/reducer_bookmark";
import searchStatusReducer from "../reducers/reducer_search_status";
import selectCharacterReducer from "../reducers/reducer_select_character";
import { loadState } from "../services/storageService";

const persistedState = loadState();

export default () => {
    const store = createStore(
        combineReducers({
            characterList: searchCharacterListReducer,
            toggle: displayReducer,
            bookmarkList: bookmarkReducer,
            searchFinished: searchStatusReducer,
            selectedCharacter: selectCharacterReducer
        }),
        persistedState,
        applyMiddleware(thunk)
    );
    return store;
};
