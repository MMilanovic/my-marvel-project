## MARVEL HEROES APP

This is app for browsing Marvel heroes.

Below you will find information on how to run the application, link to project and about assumptions of the application.


## Getting Started

Project is published on GitLab: https://gitlab.com/MMilanovic/my-marvel-project

To start this app, you need to clone the repository.
In terminal use:

git clone https://gitlab.com/MMilanovic/my-marvel-project.git

### Prerequisites

To install needed modules, inside of application directory run :

npm install 

Then, to start the application on live server:

npm start

You should now be able to see app running in your default browser.


Or if you just want to use the application without installing anything, go to :

https://marvel-heroes-app.herokuapp.com/


## About the application

Application allows users to search and bookmark their favorite Marvel characters.

Application was made using Create React App package. 

##About the style:

Application was styled to have material design look and to follow Marvel theme, using Marvel font (from Google fonts) and colors from Marvel official site.

Font Awesome icons are used to display toggle button in the Header.

##About the Javascript used:

Project is written in ES2015 syntax in React with Redux. Information about the Marvel Heroes are requested from Marvel API.
Bookmarked characters are kept in local storage in browser. This is done through persisting the state of the application to the local storage.

## About the project structure:

Organisation of the source directory of the application (/src) :

~ actions: Redux actions;

~ components : React components that are not connected to Redux store;
~ components/partials : simple React components for displaying header and footer;

~ containers : React components connected to Redux store;

~ entities : Constructor Character;

~ reducers : Redux reducer functions;

~ services/apiService : module for fetching data from API;
~ services/storageService : module for working with local storage;

~ shared/constants : values used in applications are stored in one place, to avoid repeating the same code (DRY) ;
~ shared/utils : module for helper functions;

~ store : this is where the Redux store is configured;

~ styles : the application css gets transpiled with sass preprocessor;   

~ tests : directory for keeping test files. Testing needs yet to be implented for this application. Writing
tests (with Jest) will be considered in future.

~ index.js file is where the application gets rendered to DOM.


## Acknowledgments

Spinner used in the application is from open sourced https://loading.io/css/.

## Authors

* **Maja Milanovic** 